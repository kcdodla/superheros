//
//  Constants.swift
//  SuperHeros
//
//  Created by kdodla on 11/13/19.
//  Copyright © 2019 Krishna. All rights reserved.
//

import Foundation
import CoreGraphics

struct HerosConstants {
    static let fetchHerosEndPoint = "https://bitbucket.org/dttden/mobile-coding-challenge/raw/2ee8bd47703c62c5d217d9fb9e0306922a34e581/"
    static let startYear = 2006
    static let endYear = 2018
    static let maxRows = 3
    static let defaultHeroRowHeight: CGFloat = 130
    static let defaultHeroHeaderHeight: CGFloat = 50
    static let heroCellIdentifier = "HeroTableViewCell"
    
}
