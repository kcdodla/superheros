//
//  SuperHerosListTableViewCell.swift
//  SuperHeros
//
//  Created by kdodla on 11/13/19.
//  Copyright © 2019 Krishna. All rights reserved.
//

import UIKit

protocol HeroCellData {
    var heroName: String {get}
    var heroImage: String {get}
    var heroScore: Int {get}
}

class SuperHerosListTableViewCell: UITableViewCell {

    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var heroNameLabel: UILabel!
    @IBOutlet weak var heroScoreLabel: UILabel!
    let imageLoader = ImageCacheLoader()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func display(hero: HeroCellData) {
        heroNameLabel.text = hero.heroName
        heroScoreLabel.text = String(hero.heroScore)
        let imageEndPoint = hero.heroImage.replacingOccurrences(of: ".jpeg", with: ".jpg")
        imageLoader.obtainImageWithPath(imagePath: HerosConstants.fetchHerosEndPoint+imageEndPoint) { (image) in
            self.heroImageView.image = image
        }
    }
}
