//
//  SuperHerosListViewController.swift
//  SuperHeros
//
//  Created by Krishna on 11/13/19.
//  Copyright © 2019 Krishna. All rights reserved.
//

import UIKit

class SuperHerosListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var viewModel = SuperHerosListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.tableView = self.tableView
        viewModel.viewController = self
        viewModel.titleLabel = self.titleLabel
        viewModel.registerForCell()
        viewModel.currentYear = HerosConstants.startYear
    }
    
    //To show no internet alert, we need to make API call after view appeared
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getHeros()
    }
}

extension SuperHerosListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return HerosConstants.defaultHeroRowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HerosConstants.maxRows > viewModel.totalHeros ? viewModel.totalHeros : HerosConstants.maxRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.createHeroCell(at: indexPath)
    }
}
