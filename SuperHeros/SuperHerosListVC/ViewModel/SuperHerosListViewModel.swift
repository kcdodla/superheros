//
//  SuperHerosListViewModel.swift
//  SuperHeros
//
//  Created by kdodla on 11/13/19.
//  Copyright © 2019 Krishna. All rights reserved.
//

import UIKit

struct HeroDisplayModel: HeroCellData {
    var heroName: String = ""
    var heroImage: String = ""
    var heroScore: Int = 0
}

class SuperHerosListViewModel {
    var tableView: UITableView!
    var heros = [SuperHerosListModel.SuperHero]()
    var viewController: UIViewController!
    var titleLabel: UILabel!
    var currentYear = 0
    var lastLoadedYear = 0
    var refreshControl = UIRefreshControl()
    var vSpinner : UIView?
    
    init() {
        //initializer
    }
    
    func registerForCell() {
        self.tableView.register(UINib(nibName: "SuperHerosListTableViewCell", bundle: nil), forCellReuseIdentifier: HerosConstants.heroCellIdentifier)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.refreshControl.attributedTitle = NSAttributedString(string: "Loading Super Heros")
        self.refreshControl.addTarget(self, action: #selector(loadNextSetSuperHeros), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl)
        self.tableView.tableFooterView = UIView()
    }

    @objc func loadNextSetSuperHeros() {
        if currentYear == HerosConstants.endYear {
            currentYear = HerosConstants.startYear
        } else {
            currentYear = currentYear + 3
        }
        getHeros()
    }
    
    var totalHeros: Int {
        return heros.count
    }
    
    func createHeroCell(at indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: HerosConstants.heroCellIdentifier, for: indexPath) as? SuperHerosListTableViewCell {
            if let cellData = hero(at: indexPath.row) {
                cell.display(hero: cellData)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func hero(at index: Int) -> HeroCellData?{
        if index >= totalHeros {
            return nil
        }
        
        let hero = heros[index]
        let model = heroCellData(for: hero)
        return model
    }
    
    func heroCellData(for hero: SuperHerosListModel.SuperHero) -> HeroDisplayModel {
        var model = HeroDisplayModel()
        model.heroName = hero.Name
        model.heroImage = hero.Picture
        model.heroScore = hero.Score
        return model
    }
}

extension SuperHerosListViewModel {
    func getHeros() {
        if !Reachability.isConnectedToNetwork(){
            currentYear = lastLoadedYear
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.tableView.contentOffset = CGPoint.zero
            }
            showAlertWithOk(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", vc: self.viewController)
            return
        }
        self.showSpinner(onView: self.viewController.view)
        guard let url = URL(string: HerosConstants.fetchHerosEndPoint+"\(currentYear)"+".json") else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.removeSpinner()
            }
            guard let data = data, error == nil else {
                return
            }
            do {
                self.lastLoadedYear = self.currentYear
                let jsondata = try JSONDecoder().decode(SuperHerosListModel.self, from: data)
                self.heros = jsondata.Heroes.sorted(by: >)
                DispatchQueue.main.async {
                    self.titleLabel.text = "\(self.currentYear)"
                    self.tableView.reloadData()
                }
            } catch let error {
                print(error)
            }
        }
        task.resume()
    }
}

extension SuperHerosListViewModel {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let activityIndicatorView = UIActivityIndicatorView.init(style: .whiteLarge)
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(activityIndicatorView)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        })
    }
}

extension SuperHerosListViewModel {
    func showAlertWithOk(title: String, message: String, vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        vc.present(alert, animated: true, completion: nil)
    }
}
