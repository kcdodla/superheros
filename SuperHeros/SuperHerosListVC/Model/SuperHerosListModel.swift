//
//  SuperHerosListModel.swift
//  SuperHeros
//
//  Created by kdodla on 11/13/19.
//  Copyright © 2019 Krishna. All rights reserved.
//

import Foundation

struct SuperHerosListModel: Codable {
    var Heroes: [SuperHero]
    
    struct SuperHero: Codable, Comparable {
        var Name: String
        var Picture: String
        var Score: Int
        
        static func < (lhs: SuperHerosListModel.SuperHero, rhs: SuperHerosListModel.SuperHero) -> Bool {
            return lhs.Score < rhs.Score
        }
    }
}
